const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.bjgmm.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// if a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful occured, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// Create a Task schema

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		// Default values are the predefined alues for a field
		default : "pending"
	}
})

// Create Models
// Server > Schema > Database > Colection (MongoDB)
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the documents name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){
			// Returns a message to the client/postman
			return res.send("Duplicate task found");
		}
		// If no document found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, saved) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Create a GET Request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// if an error occured
		if(err){
			//wil print any errors found in the console
			return console.error(err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// ACTIVITY
// User schema

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

const user = mongoose.model("user", userSchema);

app.post("/signup", (req, res) => {
	user.findOne({username : req.body.username}, (err, result) => {
		if(req.body.username == "" || req.body.password == ""){
			return res.send("Both username and password must be entered");
		}
		if(result != null && result.username == req.body.username){
			return res.send("User already registered");
		}
		else{
			let newUser = new user({
				username : req.body.username,
				password : req.body.password
			})

			newUser.save((saveErr, saved) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New User registered");
				}
			})
		}
	})
})


app.get("/users", (req, res) => {
	user.find({}, (err, result) => {
		if(err){
			return console.error(err);
		}
		else{
			return res.status(200).json({
				users : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));

